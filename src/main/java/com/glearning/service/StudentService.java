package com.glearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glearning.dao.StudentDAO;
import com.glearning.model.Student;

@Service
public class StudentService {
	
	@Autowired
	private StudentDAO studentDAO;
	
	public StudentService(StudentDAO studentDAO) {
		this.studentDAO = studentDAO;
	}
	
	public Student saveStudent(Student student) {
		Student savedStudent = this.studentDAO.saveStudent(student);
		return savedStudent;
	}

	public List<Student> fetchAllStudents() {
		List<Student> students = this.studentDAO.fetchAllStudents();
		return students;
	}

	public Student fetchStudentById(int studentId) {
		Student student = this.studentDAO.fetchStudentById(studentId);
		return student;
	}

	public void deleteStudentById(int id) {
		this.studentDAO.deleteStudentById(id);
	}
}
