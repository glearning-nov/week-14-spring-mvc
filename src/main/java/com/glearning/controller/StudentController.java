package com.glearning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.glearning.model.Student;
import com.glearning.service.StudentService;

@Controller
@RequestMapping("/students")
public class StudentController {
	
	@Autowired
	private final StudentService studentService;
	
	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}
	
	public ModelAndView saveStudent(ModelAndView modelAndView) {
		Student student = new Student();
		modelAndView.addObject("student", student);
		modelAndView.setViewName("create-student");
		return modelAndView;
	}
	
	@RequestMapping("/registrationForm")
	public String showRegistrationForm(Model theModel) {

		Student student = new Student();

		theModel.addAttribute("studentModel", student);

		return "employee-form";
	}

}
