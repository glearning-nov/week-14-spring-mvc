package com.glearning.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.glearning.model.Student;

@Repository
public class StudentDAO {

	@Autowired
	private final SessionFactory sessionFactory;

	public StudentDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Student saveStudent(Student student) {
		Session session = sessionFactory.openSession();
		session.save(student);
		return student;
	}

	public List<Student> fetchAllStudents() {
		Session session = sessionFactory.openSession();
		List<Student> students = session.createQuery("FROM Student", Student.class).list();
		return students;
	}

	public Student fetchStudentById(int studentId) {
		Session session = sessionFactory.openSession();
		Student student = session.get(Student.class, studentId);
		return student;
	}

	public void deleteStudentById(int id) {
		Session session = sessionFactory.openSession();

		Student student = session.get(Student.class, id);
		if (student != null) {
			session.delete(student);
		}
	}
}